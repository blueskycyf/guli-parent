package com.xunqi.oss.service.impl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.xunqi.oss.service.OssService;
import com.xunqi.oss.util.ConstantPropertiesUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-07-28 21:11
 **/

@Service
public class OssServiceImpl implements OssService {

    @Autowired
    private ConstantPropertiesUtils utils;

    /**
     * 上传头像到oss
     * @param file
     * @return
     */
    @Override
    public String uploadFileAvatar(MultipartFile file) {

        // Endpoint以杭州为例，其它Region请按实际情况填写。
        String endpoint = utils.getEndpoint();
        // 云账号AccessKey有所有API访问权限，建议遵循阿里云安全最佳实践，创建并使用RAM子账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建。
        String accessKeyId = utils.getKeyId();
        String accessKeySecret = utils.getKeySecret();

        String bucketName = utils.getBucketName();

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        // 上传文件流。
        InputStream inputStream = null;
        try {
            inputStream = file.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //获取文件名称
        String fileName = file.getOriginalFilename();

        //1、在文件名称里面添加唯一的值
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        fileName = uuid + fileName;

        //2、把文件按照日期进行分类
        //获取当前的日期
        String dataPath = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        // String dataPath = new DateTime().toString("yyyy/MM/dd");

        fileName = dataPath + "/" + fileName;
        //调用oss方法api实现上传
        //第一个参数：BucketName
        //第二个参数：上传到oss文件路径和文件名称
        //第三个参数：上传文件输入流
        ossClient.putObject(bucketName, fileName, inputStream);

        // 关闭OSSClient。
        ossClient.shutdown();

        //把上传之后文件路径进行返回
        //需要把上传到阿里云oss路径手动拼接出来
        String url = "https://" + bucketName + "." + endpoint + "/" + fileName;

        return url;
    }
}
