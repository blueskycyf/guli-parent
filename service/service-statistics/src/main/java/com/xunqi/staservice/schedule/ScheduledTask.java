package com.xunqi.staservice.schedule;

import com.xunqi.staservice.service.StatisticsDailyService;
import com.xunqi.staservice.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Description: 定时任务
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-13 15:07
 **/

@Component
public class ScheduledTask {

    @Autowired
    private StatisticsDailyService staService;

    /**
     * 测试
     * 每天七点到二十三点每五秒执行一次
     */
    //@Scheduled(cron = "0/5 * * * * ?")
    // @Schedules({
    //         @Scheduled(cron = "0/5 * * * * ?")
    // })
    // public void task1() {
    //     System.out.println("*************task1执行了");
    // }


    public void task2() {
        System.out.println("************task2执行了");
        //获取上一天的日期
        String day = DateUtil.formatDate(DateUtil.addDays(new Date(), -1));
        staService.registerCount(day);
    }

}
