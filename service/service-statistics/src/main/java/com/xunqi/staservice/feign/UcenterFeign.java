package com.xunqi.staservice.feign;

import com.xunqi.commonutils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-13 14:06
 **/

@Component
@FeignClient("service-ucenter")
public interface UcenterFeign {

    @GetMapping(value = "/educenter/member/countRegister/{day}")
    R countRegister(@PathVariable("day") String day);

}
