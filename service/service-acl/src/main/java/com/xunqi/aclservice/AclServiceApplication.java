package com.xunqi.aclservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-14 14:16
 **/

@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan("com.xunqi")
@MapperScan(basePackages = {"com.xunqi.aclservice.mapper"})
public class AclServiceApplication {

    public static void main(String[] args) {

        SpringApplication.run(AclServiceApplication.class, args);

    }

}
