package com.xunqi.educms.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xunqi.commonutils.R;
import com.xunqi.educms.entity.CrmBanner;
import com.xunqi.educms.service.CrmBannerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 首页banner表 前端控制器
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-08-06
 */

@Api(value = "前台Banner管理接口",tags = "前台Banner管理接口")
@RestController
@RequestMapping("/educms/bannerfront")
public class BannerFrontController {

    @Autowired
    private CrmBannerService bannerService;

    @Cacheable(key = "'selectIndexList'",value = "guli_banner")
    @ApiOperation(value = "获取Banner列表",notes = "获取Banner列表",httpMethod = "GET")
    @GetMapping(value = "/getAllBanner")
    public R getAllBanner() {

        //先根据id进行降序排列，显示排序前两条记录
        QueryWrapper<CrmBanner> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByDesc("id");
        queryWrapper.last("limit 2");

        List<CrmBanner> list = bannerService.list(queryWrapper);

        return R.ok().data("list",list);
    }

}

