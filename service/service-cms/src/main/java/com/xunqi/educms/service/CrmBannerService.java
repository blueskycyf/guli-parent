package com.xunqi.educms.service;

import com.xunqi.educms.entity.CrmBanner;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 首页banner表 服务类
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-08-06
 */
public interface CrmBannerService extends IService<CrmBanner> {

    /**
     * 查询所有的banner
     * @return
     */
    List<CrmBanner> selectAllBanner();

}
