package com.xunqi.educenter.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xunqi.educenter.entity.UcenterMember;
import com.xunqi.educenter.vo.RegisterVo;
import com.xunqi.educenter.vo.UcenterMemberVo;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-08-08
 */
public interface UcenterMemberService extends IService<UcenterMember> {

    /**
     * 用户登录
     * @param memberVo
     * @return
     */
    String login(UcenterMemberVo memberVo);

    /**
     * 用户注册
     * @param registerVo
     */
    void register(RegisterVo registerVo);

    /**
     * 根据 oppenid 查询用户信息
     * @param openid
     * @return
     */
    UcenterMember getByOpenid(String openid);

    /**
     *
     * @param day
     * @return
     */
    Integer countRegister(String day);
}
