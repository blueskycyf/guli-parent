package com.xunqi.orderservice.feign.impl;

import com.xunqi.commonutils.R;
import com.xunqi.orderservice.feign.CourseFeign;
import com.xunqi.orderservice.vo.CourseVo;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-12 16:07
 **/

@Component
public class CourseFeignImpl implements CourseFeign {

    @Override
    public CourseVo getCourseInfo(String courseId) {
        return null;
    }

    @Override
    public R addBuyCount(String courseId) {

        return R.error().message("订单出现异常");
    }
}
