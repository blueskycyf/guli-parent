package com.xunqi.orderservice.mapper;

import com.xunqi.orderservice.entity.PayLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 支付日志表 Mapper 接口
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-08-12
 */
public interface PayLogMapper extends BaseMapper<PayLog> {

}
