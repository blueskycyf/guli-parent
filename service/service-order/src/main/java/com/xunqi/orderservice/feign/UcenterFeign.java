package com.xunqi.orderservice.feign;

import com.xunqi.orderservice.feign.impl.UcenterFeignImpl;
import com.xunqi.orderservice.vo.UcenterMemberVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-12 15:19
 **/

@Component
@FeignClient(value = "service-ucenter",fallback = UcenterFeignImpl.class)
public interface UcenterFeign {

    @GetMapping(value = "/educenter/member/getByTokenUcenterInfo")
    UcenterMemberVo getByTokenUcenterInfo(@RequestParam("jwtToken") String jwtToken);

}
