package com.xunqi.eduservice.service;

import com.xunqi.eduservice.entity.EduVideo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程视频 服务类
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-07-31
 */
public interface EduVideoService extends IService<EduVideo> {

    boolean removeVideoByCourseId(String courseId);

    EduVideo getVideoSourceIdByInfo(String videoSourceId);

}
