package com.xunqi.eduservice.mapper;

import com.xunqi.eduservice.entity.EduChapter;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-07-31
 */
public interface EduChapterMapper extends BaseMapper<EduChapter> {

}
