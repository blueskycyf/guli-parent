package com.xunqi.eduservice.controller.front;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xunqi.commonutils.R;
import com.xunqi.eduservice.entity.EduCourse;
import com.xunqi.eduservice.entity.EduTeacher;
import com.xunqi.eduservice.service.EduCourseService;
import com.xunqi.eduservice.service.EduTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-07 11:05
 **/

@RestController
@RequestMapping(value = "/eduservice/indexfront")
public class IndexFrontController {

    @Autowired
    private EduCourseService courseService;

    @Autowired
    private EduTeacherService teacherService;

    /**
     * 查询前8条热门课程，查询前4条名师记录
     * @return
     */
    @Cacheable(key = "'index'",value = "guli_banner")
    @GetMapping(value = "/index")
    public R index() {
        //查询前8条热门课程
        QueryWrapper<EduCourse> courseQueryWrapper = new QueryWrapper<>();
        courseQueryWrapper.orderByDesc("id");
        courseQueryWrapper.last("limit 8");
        List<EduCourse> eduCourseList = courseService.list(courseQueryWrapper);

        //查询前4条名师记录
        QueryWrapper<EduTeacher> teacherQueryWrapper = new QueryWrapper<>();
        teacherQueryWrapper.orderByDesc("id");
        teacherQueryWrapper.last("limit 4");
        List<EduTeacher> teacherList = teacherService.list(teacherQueryWrapper);
        System.out.println(teacherList.size());
        System.out.println(eduCourseList.size());
        return R.ok().data("eduCourseList",eduCourseList).data("teacherList",teacherList);

    }

}
