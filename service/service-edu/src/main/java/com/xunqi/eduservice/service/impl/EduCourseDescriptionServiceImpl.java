package com.xunqi.eduservice.service.impl;

import com.xunqi.eduservice.entity.EduCourseDescription;
import com.xunqi.eduservice.mapper.EduCourseDescriptionMapper;
import com.xunqi.eduservice.service.EduCourseDescriptionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程简介 服务实现类
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-07-31
 */
@Service
public class EduCourseDescriptionServiceImpl extends ServiceImpl<EduCourseDescriptionMapper, EduCourseDescription> implements EduCourseDescriptionService {

}
