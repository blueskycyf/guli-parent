package com.xunqi.eduservice.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xunqi.eduservice.entity.EduSubject;
import com.xunqi.eduservice.entity.excel.SubjectData;
import com.xunqi.eduservice.service.EduSubjectService;
import com.xunqi.servicebase.exception.GuliException;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-07-29 15:49
 **/
public class  SubjectExcelListener extends AnalysisEventListener<SubjectData> {

    public EduSubjectService eduSubjectService;

    public SubjectExcelListener() { }

    public SubjectExcelListener(EduSubjectService eduSubjectService) {
        this.eduSubjectService = eduSubjectService;
    }


    @Override
    public void invoke(SubjectData data, AnalysisContext context) {
        if (data == null) {
            throw new GuliException(20001,"文件数据为空");
        }

        //一行一行读取，每次读取有两个值，第一个值一级分类，第二个值二级分类
        EduSubject existOneSubject = this.existOneSubject(eduSubjectService, data.getOnSubjectName());
        if (existOneSubject == null) {
            //没有相同的分类，进行添加
            existOneSubject = new EduSubject();
            existOneSubject.setParentId("0");
            existOneSubject.setTitle(data.getOnSubjectName());       //设置一级分类名称
            eduSubjectService.save(existOneSubject);
        }

        //获取一级分类id值
        String pid = existOneSubject.getId();
        //添加分类，判断二级分类是否重复
        EduSubject existTwoSubject = this.existTwoSubject(eduSubjectService, data.getTwoSubjectName(), pid);
        if (existTwoSubject == null) {
            //没有相同的分类，进行添加
            existTwoSubject = new EduSubject();
            existTwoSubject.setParentId(pid);
            existTwoSubject.setTitle(data.getTwoSubjectName());       //设置二级分类名称
            eduSubjectService.save(existTwoSubject);
        }

    }

    //判断一级分类不能重复添加
    private EduSubject existOneSubject(EduSubjectService eduSubjectService,String name) {

        QueryWrapper<EduSubject> queryWrapper = new QueryWrapper<EduSubject>();
        queryWrapper.eq("title",name);
        queryWrapper.eq("parent_id","0");
        EduSubject oneSubject = eduSubjectService.getOne(queryWrapper);

        return oneSubject;

    }


    //判断二级分类不能重复添加
    private EduSubject existTwoSubject(EduSubjectService eduSubjectService,String name,String pid) {

        QueryWrapper<EduSubject> queryWrapper = new QueryWrapper<EduSubject>();
        queryWrapper.eq("title",name);
        queryWrapper.eq("parent_id",pid);

        EduSubject twoSubject = eduSubjectService.getOne(queryWrapper);

        return twoSubject;

    }


    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {

    }
}
