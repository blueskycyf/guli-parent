package com.xunqi.eduservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xunqi.eduservice.entity.EduTeacher;
import com.xunqi.eduservice.mapper.EduTeacherMapper;
import com.xunqi.eduservice.service.EduTeacherService;
import com.xunqi.eduservice.vo.TeacherQueryVo;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 讲师 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-07-26
 */
@Service
public class EduTeacherServiceImpl extends ServiceImpl<EduTeacherMapper, EduTeacher> implements EduTeacherService {

    @Override
    public IPage<EduTeacher> pageTeacherCondition(Long current, Long limit, TeacherQueryVo vo) {

        //创建page对象
        Page<EduTeacher> pageTeacher = new Page<>(current,limit);

        //构建条件
        QueryWrapper<EduTeacher> queryWrapper = new QueryWrapper<>();

        IPage<EduTeacher> teacherIPage = null;

        if (vo == null){
            teacherIPage = baseMapper.selectPage(pageTeacher, queryWrapper);
            return teacherIPage;
        }

        String name = vo.getName();
        Integer level = vo.getLevel();
        String begin = vo.getBegin();
        String end = vo.getEnd();
        if (!StringUtils.isEmpty(name)) {
            queryWrapper.like("name", name);
        }
        if (!StringUtils.isEmpty(level) ) {
            queryWrapper.eq("level", level);
        }
        if (!StringUtils.isEmpty(begin)) {
            queryWrapper.ge("gmt_create", begin);
        }
        if (!StringUtils.isEmpty(end)) {
            queryWrapper.le("gmt_create", end);
        }

        //排序
        queryWrapper.orderByDesc("gmt_create",begin);

        teacherIPage = this.page(pageTeacher, queryWrapper);

        return teacherIPage;
    }

    @Override
    public Map<String, Object> getTeacherFrontList(Page<EduTeacher> eduTeacherPage) {

        IPage<EduTeacher> teacherIPage = this.baseMapper.selectPage(
                eduTeacherPage, new QueryWrapper<EduTeacher>().orderByDesc("id"));

        List<EduTeacher> records = teacherIPage.getRecords();
        long total = teacherIPage.getTotal();
        long current = teacherIPage.getCurrent();
        long pages = teacherIPage.getPages();
        long size = teacherIPage.getSize();

        boolean hasNext = eduTeacherPage.hasNext();     //是否有下一页
        boolean hasPrevious = eduTeacherPage.hasPrevious();     //是否有上一页
        //把分页数据获取出来，放到map集合
        Map<String,Object> map = new HashMap<>();
        map.put("items",records);
        map.put("total",total);
        map.put("current",current);
        map.put("pages",pages);
        map.put("size",size);
        map.put("hasNext",hasNext);
        map.put("hasPrevious",hasPrevious);


        return map;
    }
}
