package com.xunqi.eduservice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xunqi.eduservice.entity.EduSubject;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 课程科目
 *
 * @author 夏沫止水
 * @email HeJieLin@gulimall.com
 * @date 2020-07-29 15:22:28
 */
public interface EduSubjectService extends IService<EduSubject> {

    /**
     * 添加课程分类
     * @param file
     */
    void saveSubject(MultipartFile file,EduSubjectService eduSubjectService);

    /**
     * 查询一级二级分类课程信息
     * @return
     */
    List<EduSubject> getAllOneTwoSubject();


}

