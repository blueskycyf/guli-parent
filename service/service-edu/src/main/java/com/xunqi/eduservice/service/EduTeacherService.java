package com.xunqi.eduservice.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xunqi.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xunqi.eduservice.vo.TeacherQueryVo;

import java.util.Map;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author testjava
 * @since 2020-07-26
 */
public interface EduTeacherService extends IService<EduTeacher> {

    /**
     * 多条件分页查询
     * @param current
     * @param limit
     * @param vo
     * @return
     */
    IPage<EduTeacher> pageTeacherCondition(Long current, Long limit, TeacherQueryVo vo);

    /**
     * 前台分页查询
     * @param eduTeacherPage
     * @return
     */
    Map<String, Object> getTeacherFrontList(Page<EduTeacher> eduTeacherPage);
}
