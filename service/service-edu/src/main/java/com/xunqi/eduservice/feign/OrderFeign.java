package com.xunqi.eduservice.feign;

import com.xunqi.eduservice.feign.impl.OrderFeignImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-13 09:56
 **/

@Component
@FeignClient(value = "service-order",fallback = OrderFeignImpl.class)
public interface OrderFeign {

    @GetMapping(value = "/orderservice/order/isBuyCourse/{courseId}/{memberId}")
    boolean isBuyCourse(@PathVariable("courseId") String courseId,
                               @PathVariable("memberId") String memberId);

}
